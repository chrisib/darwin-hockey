#include <LinuxDARwIn.h>
#include <iostream>
#include <ctime>

using namespace Robot;
using namespace std;

int main()
{	
	cout << "Initializing..." << endl;
	LinuxDARwIn::ChangeCurrentDir();
	LinuxDARwIn::Initialize("../motion_4096.bin",15);
    //LinuxDARwIn::InitializeSignals();
    //Voice::Initialize();
	cout << "done" << endl << endl;
	
	cout << "Standing up..." << endl;
    Action::GetInstance()->Start(160);
	while(Action::GetInstance()->IsRunning())
		usleep(8000);
    cout << "done" << endl << endl;
    
    
    Action::GetInstance()->m_Joint.SetEnableBody(true,true);
    Action::GetInstance()->m_Joint.SetEnableHeadOnly(false);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    Head::GetInstance()->MoveByAngle(90,5);
    
    cout << "Will shoot in 10 seconds..." << endl;
    time_t t = time(NULL);
    while(time(NULL)-t < 10)
    {
		usleep(1000);
	}
    cout << "Shooting now" << endl;
    
    Action::GetInstance()->Start(161);
    while(Action::GetInstance()->IsRunning())
		usleep(8000);
        
    return 0;
}
