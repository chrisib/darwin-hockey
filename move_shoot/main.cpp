#include <LinuxDARwIn.h>
#include <iostream>
#include "Skating.h"
#include <darwin/framework/Voice.h>
#include <darwin/framework/SingleTarget.h>
#include <darwin/linux/CvCamera.h>
#include <cstring>
#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <cstring>

using namespace Robot;
using namespace std;
using namespace cv;

#define FOREHAND_SHOT_READY 150
#define FOREHAND_SHOT_GO    151
#define BACKHAND_SHOT_READY 160
#define BACKHAND_SHOT_GO    161

#define SKATE_HEIGHT        2.5

//#define FAKE_AIM

typedef enum ARG_STATUS_T {
    ARGS_OK,
    ARGS_BAD
} ArgStatus;

typedef enum MODE_T {
    MODE_NORMAL,
    MODE_VIS_TEST
} ProgramMode;

typedef enum STATE_T {
    STATE_IDLE,
    STATE_MOVING,
    STATE_STOPPING,
    STATE_SHOOTING
} ProgramState;

SingleTarget *target;
SingleTarget *stick;

minIni *ini = new minIni("config.ini");

double MIN_RANGE = ini->getd("Vision","MinRange");
double MAX_RANGE = ini->getd("Vision","MaxRange");
double THRESHOLD_ANGLE = ini->getd("Vision","Threshold");
double LEFT_ANGLE = ini->getd("Vision","Left");
double RIGHT_ANGLE = ini->getd("Vision","Right");

VideoWriter rawStream = VideoWriter (ini->gets("Files","Raw"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
VideoWriter ballStream = VideoWriter (ini->gets("Files","Ball"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);

//VideoWriter stickStream = VideoWriter (ini->gets("Files","Stick"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);

bool hide_windows = false;
int mode = MODE_NORMAL;
int state = STATE_IDLE;

pthread_t videoThreadId;

double range = 0;
double angle = 0;

void pauseThread()
{
	pthread_yield();
}

void handleVideo()
{
    static char buffer[255];
    static cv::Point pt;
    static cv::Scalar colour(255,255,255);
    
	CvCamera::GetInstance()->CaptureFrame();
	Mat blur = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
	Mat ballScanline = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
	//Mat stickScanline = Mat::zeros(CvCamera::GetInstance()->img.rows,CvCamera::GetInstance()->img.cols,CvCamera::GetInstance()->img.type());
	GaussianBlur(CvCamera::GetInstance()->yuvFrame, blur, Size(3,3), 0);
	
	if(target->FindInFrame(blur,&ballScanline))
	{
		range = target->GetBoundingBox()->GetRange(SKATE_HEIGHT);
		angle = target->GetBoundingBox()->GetAngle(SKATE_HEIGHT);
		//cout << range << " " << angle << " " << Head::GetInstance()->GetPanAngle() << endl;
		
		target->DrawBoundingBox(CvCamera::GetInstance()->rgbFrame);
		target->DrawBoundingBox(ballScanline);
		//target->Print();
		
		char *lr;
		char *fb;
		
		if(angle > LEFT_ANGLE)
			lr = "<--";
		else if(angle <= LEFT_ANGLE && angle >= THRESHOLD_ANGLE)
			lr = "|<|";
		else if(angle <= THRESHOLD_ANGLE && angle >= RIGHT_ANGLE)
			lr = "|>|";
		else
			lr = "-->";
			
		if(range < MIN_RANGE)
			fb = "v";
		else if(range >= MIN_RANGE && range <= MAX_RANGE)
			fb = "--";
		else
			fb = "^";
        
        // annotate the ball's range and angle data into the frame
        sprintf(buffer, "Range: %0.2fmm %s", range, fb);
        pt = cv::Point(0,12);
        cv::putText(ballScanline, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        
        sprintf(buffer, "Angle: %0.2f %s", angle, lr);
        pt = cv::Point(0,24);
        cv::putText(ballScanline, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
	}
    else
    {
        // placeholder text
        pt = cv::Point(0,12);
        cv::putText(ballScanline, "Range:",  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        
        pt = cv::Point(0,24);
        cv::putText(ballScanline, "Angle:",  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
    }
	
	//if(stick->FindInFrame(blur,&stickScanline))
	//{
	//	stick->DrawBoundingBox(CvCamera::GetInstance()->img);
	//}
    
    rawStream << CvCamera::GetInstance()->rgbFrame;
    //stickStream << stickScanline;
    ballStream << ballScanline;
	
	if(!hide_windows)
	{
		cv::imshow("Webcam",CvCamera::GetInstance()->rgbFrame);
		cv::imshow("Ball",ballScanline);
		//cv::imshow("Stick",stickScanline);
		cv::waitKey(1);
	}
}

void *videoLoop(void* arg)
{
	for(;;)
    {
		handleVideo();
		
		// sleep 10 ms
        MotionManager::GetInstance()->msleep(4,pauseThread);
	}

    pthread_exit(NULL);
    return NULL; // Unreachable
}

void handleButton()
{
	static int lastButton = MotionStatus::BUTTON;
	
	if(MotionStatus::BUTTON==lastButton)
		return;
	else
	{
		cout << "[INFO] Button pressed" << endl;
		lastButton = MotionStatus::BUTTON;
		
		if(lastButton == CM730::MIDDLE_BUTTON)
		{
			cout << "[INFO] Skating module started" << endl;
			state = STATE_MOVING;
		}
		else if(state!=STATE_IDLE && lastButton == CM730::LEFT_BUTTON)
		{
			cout << "[INFO] Entering idle state" << endl;
			state = STATE_IDLE;
		}
	}
}

int parseArgs(int argc, char** argv)
{
    for(int i=1; i<argc; i++)
	{
		if(argc>1 && !strcmp("--no-video",argv[i]))
		{
			hide_windows = true;
		}
		else if(argc>1 && !strcmp("--vision-test",argv[1]))
		{
			mode = MODE_VIS_TEST;
			hide_windows = false;
		}
		else
		{
			cout << endl << "Usage:" << endl <<
			        "\thockey [--no-video] [--vision-test] [--help]" << endl << endl;
			return ARGS_BAD;
		}
	}
    
    return ARGS_OK;
}

void initialize()
{
    cout << "Initializing Camera..." << endl;
	CvCamera::GetInstance()->Initialize(0);
	CvCamera::GetInstance()->LoadINISettings(ini);
	if(!hide_windows)
	{
		cv::namedWindow("Webcam",1);
		cv::namedWindow("Ball",1);
		//cv::namedWindow("Stick",1);
		cvMoveWindow("Webcam",0,0);
		cvMoveWindow("Ball",0,300);
		//cvMoveWindow("Stick",325,300);
	}
	cout << "Done" << endl;
	
    
    cout << "Initializing targets..." << endl;
    target = new SingleTarget("Ball");
	target->LoadIniSettings(*ini,"Yellow");
	stick = new SingleTarget("Stick");
	stick->LoadIniSettings(*ini,"Red");
	cout << "done" << endl;
    
    if(mode==MODE_VIS_TEST)
	{
		cout << "Vision test mode" << endl;
	}
    else
    {
        cout << "Initializing Framework..." << endl;
        LinuxDARwIn::ChangeCurrentDir();
        LinuxDARwIn::Initialize("../motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
        //LinuxDARwIn::InitializeSignals();
        Voice::Initialize("en-us+f2");
        cout << "done" << endl << endl;
        
        cout << "Standing up..." << endl;
        Action::GetInstance()->Start(FOREHAND_SHOT_READY);
        while(Action::GetInstance()->IsRunning())
            usleep(8000);
        cout << "done" << endl << endl;
        
        cout << "Setting up Skating module..." << endl;
        Skating::GetInstance()->m_Joint.SetEnableBody(false);
        Skating::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
        MotionManager::GetInstance()->AddModule((MotionModule*)Skating::GetInstance());
        Skating::GetInstance()->LoadINISettings(ini);
        //int MIN_ROW = ini->geti("Ball","Top");
        //int MAX_ROW = ini->geti("Ball","Bottom");
        //int MIN_COL = ini->geti("Ball","Left");
        //int MAX_COL = ini->geti("Ball","Right");
        //int LR_THRESHOLD = ini->geti("Ball","LRThreshold");
        cout << "done" << endl << endl;
        
        Skating::GetInstance()->X_MOVE_AMPLITUDE = ini->geti("Movement","StrideLength");
        Skating::GetInstance()->A_MOVE_AMPLITUDE = ini->geti("Movement","Turn");
        
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(0,-10);
        
        cout << "Starting video thread..." << endl;
        pthread_create(&videoThreadId, NULL, &videoLoop, NULL);
        cout << "Done" << endl;
    }
}

int main(int argc, char** argv)
{	
	if(parseArgs(argc, argv) == ARGS_BAD)
        return 0;
	
    initialize();
	
    if(mode==MODE_VIS_TEST)
    {
        for(;;)
            handleVideo();
    }
    else
    {
        
        cout << "Parameters:" << endl <<
                "\tSwing : " << ini->geti("Skating Config","swing_right_left") << endl <<
                "\tYaw   : " << ini->geti("Skating Config","yaw_offset") << endl <<
                "\tStride: " << ini->geti("Movement","StrideLength") << endl <<
                "\tTurn  : " << ini->geti("Movement","Turn") << endl <<
                endl;
        
        cout << "Press the middle button to start" << endl;
        Voice::Speak("Press the middle button");
        MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);
        state = STATE_MOVING;
        
        // keep hitting the ball forever
        for(;;)
        {
            handleButton();
            
            switch(state)
            {
                case STATE_IDLE:
                    Skating::GetInstance()->Stop();
                    break;
                
                case STATE_MOVING:
                    Skating::GetInstance()->Start();
                    
                    if(target->WasFound())
                    {	
                        if(range >= MIN_RANGE && range <= MAX_RANGE &&
                            angle <= LEFT_ANGLE && angle >= RIGHT_ANGLE)
                        {
                            cout << "Target might be in position; stopping" << endl;
                            // stop skating and get ready to shoot
                            state = STATE_STOPPING;
                            
                        }
                        
                    }// wasFound()
                    
                    break;
                    
                case STATE_STOPPING:
                    Skating::GetInstance()->Stop();
                    Skating::GetInstance()->Finish();
                    
                    state = STATE_SHOOTING;
                    break;
                    
                case STATE_SHOOTING:
#ifdef FAKE_AIM
                    // found the puck
                    // make it look like we're aiming and then swing
                    time_t t;
                    Head::GetInstance()->MoveByAngle(-90,0);
                    t = time(NULL);
                    while(time(NULL)-t<1)
                        pauseThread();
                    
                    Head::GetInstance()->MoveByAngle(0,0);
                    t = time(NULL);
                    while(time(NULL)-t<1)
                        pauseThread();
                    
                    Head::GetInstance()->MoveByAngle(-90,0);
                    t = time(NULL);
                    while(time(NULL)-t<1)
                        pauseThread();
#endif

                    // swing the stick and hit the ball
                    if(angle > THRESHOLD_ANGLE)
                    {
                        //Voice::Speak("Shooting Backhand");
                        cout << "Shooting Backhand" << endl;
                        Action::GetInstance()->Start(BACKHAND_SHOT_GO);
                    }
                    else
                    {
                        //Voice::Speak("Shooting Forehand");
                        cout << "Shooting Forehand" << endl;
                        Action::GetInstance()->Start(FOREHAND_SHOT_GO);
                    }
                    Action::GetInstance()->Finish();
                    cout << "done" << endl;
                    
                    //Action::GetInstance()->Start(FOREHAND_SHOT_READY);
                    //Head::GetInstance()->MoveByAngle(0,0);
                    
                    state = STATE_MOVING;
                    break;
                
            }
        }// for(;;)
    }
    return 0;
}
