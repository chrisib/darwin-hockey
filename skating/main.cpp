#include <LinuxDARwIn.h>
#include <iostream>
#include "Skating.h"
#include <darwin/framework/Voice.h>
#include <darwin/framework/Target.h>
#include <darwin/linux/YuvCamera.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftArm.h>
#include <cstring>

using namespace Robot;
using namespace std;
using namespace cv;

#define FOREHAND_SHOT_READY 150
#define FOREHAND_SHOT_GO    151
#define BACKHAND_SHOT_READY 160
#define BACKHAND_SHOT_GO    161

//#define FAKE_AIM

Target *target;
Target *stick;

minIni *ini;// = new minIni("config.ini");

int main(int argc, char** argv)
{	
	if(argc>1 && strcmp(argv[1],"--help"))
	{
		cout << "Loading config from file " << argv[1] << endl;
		ini=new minIni(argv[1]);
	}
	else if(argc>1)
	{
		cout << "Usage:\n\t" << argv[0] << " [--help|CONFIG_FILE]" << endl << endl;
		return 0;
	}
	else
		ini=new minIni("config.ini");
		
	
    cout << "Initializing Framework..." << endl;
	LinuxDARwIn::ChangeCurrentDir();
	LinuxDARwIn::Initialize("../motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
    //LinuxDARwIn::InitializeSignals();
    Voice::Initialize("en-us+f2");
	cout << "done" << endl << endl;
	
	cout << "Standing up..." << endl;
    Action::GetInstance()->Start(FOREHAND_SHOT_READY);
	while(Action::GetInstance()->IsRunning())
		usleep(8000);
    cout << "done" << endl << endl;
    
    cout << "Setting up Skating module..." << endl;
    MotionManager::GetInstance()->AddModule((MotionModule*)Skating::GetInstance());
    MotionManager::GetInstance()->AddModule((MotionModule*)LeftArm::GetInstance());
    MotionManager::GetInstance()->AddModule((MotionModule*)RightArm::GetInstance());
    Skating::GetInstance()->LoadINISettings(ini);
    cout << "done" << endl << endl;
    
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
	Head::GetInstance()->MoveByAngle(0,0);
    
	//Skating::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
	Skating::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
	LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
	RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);
	LeftArm::GetInstance()->RelaxHand();
	RightArm::GetInstance()->RelaxHand();
	Skating::GetInstance()->X_MOVE_AMPLITUDE = ini->geti("Movement","StrideLength");
	Skating::GetInstance()->A_MOVE_AMPLITUDE = ini->geti("Movement","Turn");
	
	cout << "Parameters:" << endl <<
	        "\tSwing : " << ini->geti("Skating Config","swing_right_left") << endl <<
	        "\tYaw   : " << ini->geti("Skating Config","yaw_offset") << endl <<
	        "\tStride: " << ini->geti("Movement","StrideLength") << endl <<
	        "\tTurn  : " << ini->geti("Movement","Turn") << endl <<
	        endl;
	
	// keep hitting the ball forever
	for(;;)
	{
		cout << "Press the middle button to start" << endl;
		MotionManager::WaitButton(CM730::MIDDLE_BUTTON);
		Skating::GetInstance()->Start();
		Skating::GetInstance()->X_MOVE_AMPLITUDE = ini->geti("Movement","StrideLength");
		Skating::GetInstance()->A_MOVE_AMPLITUDE = ini->geti("Movement","Turn");
		
		cout << "Press the left button to stop" << endl;
		MotionManager::WaitButton(CM730::LEFT_BUTTON);
		Skating::GetInstance()->Stop();
	}
        
    return 0;
}
