#include <LinuxDARwIn.h>
#include <iostream>
#include <ctime>

using namespace Robot;
using namespace std;

int main()
{	
	cout << "Initializing..." << endl;
	LinuxDARwIn::ChangeCurrentDir();
	LinuxDARwIn::Initialize("../motion_4096.bin",15);
    //LinuxDARwIn::InitializeSignals();
    //Voice::Initialize();
	cout << "done" << endl << endl;
	
	cout << "Loading ini..." << endl;
    minIni *ini = new minIni("config.ini");
    cout << "done" << endl << endl;
	
	cout << "Standing up..." << endl;
    Action::GetInstance()->Start(170);
	while(Action::GetInstance()->IsRunning())
		usleep(8000);
    cout << "done" << endl << endl;
    
    
    Action::GetInstance()->m_Joint.SetEnableBody(true,true);
    Action::GetInstance()->m_Joint.SetEnableHeadOnly(false);
    Action::GetInstance()->m_Joint.SetEnableLowerBody(false);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    Walking::GetInstance()->LoadINISettings(ini,"Skating Config");
    Walking::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
    Head::GetInstance()->MoveByAngle(0,-5);
    
    Walking::GetInstance()->X_MOVE_AMPLITUDE = 5;
    
    cout << "Will start dribbling in 5 seconds" << endl;
    time_t t = time(NULL);
    while(time(NULL)-t < 5)
    {
		usleep(1000);
	}
    cout << "GO!" << endl;
    
    Walking::GetInstance()->Start();
    
    Action::GetInstance()->Start(171);
    
    for(;;);
        
    return 0;
}
