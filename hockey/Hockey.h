#ifndef HOCKEY_H
#define HOCKEY_H

#include <darwin/framework/Application.h>
#include <darwin/framework/SingleBlob.h>
#include <opencv2/opencv.hpp>

// height of skate in cm
#define SKATE_HEIGHT        2.5

class Hockey : public Robot::Application
{
public:
    Hockey();
    virtual ~Hockey();

    virtual bool Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition);
    virtual void LoadIniSettings();
    virtual void Process();
    virtual void Execute();

    virtual void HandleVideo();

private:
    typedef enum STATE_T {
        STATE_IDLE,
        STATE_MOVING,
        STATE_STOPPING,
        STATE_SHOOTING
    } ProgramState;
    int state;

    Robot::SingleBlob ball;

    // position of the ball
    double range, angle;

    double MIN_RANGE;
    double MAX_RANGE;
    double THRESHOLD_ANGLE;
    double LEFT_ANGLE;
    double RIGHT_ANGLE;

    bool recordVideo;
    cv::VideoWriter *ballStream;
    cv::VideoWriter *rawStream;

    bool enableLog;

    int lastButton;
    void handleButton();
};

#endif // HOCKEY_H
