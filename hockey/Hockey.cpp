#include "Hockey.h"
#include "Skating.h"
#include <iostream>
#include <darwin/framework/MotionManager.h>
#include <darwin/framework/MotionStatus.h>
#include <darwin/framework/Head.h>
#include <darwin/framework/Voice.h>

using namespace Robot;
using namespace std;
using namespace cv;

#define FOREHAND_SHOT_READY 150
#define FOREHAND_SHOT_GO    151
#define BACKHAND_SHOT_READY 160
#define BACKHAND_SHOT_GO    161


Hockey::Hockey()
{
    RegisterSwitch("--record",&recordVideo,false,"Record the video streams");
    RegisterSwitch("--log",&enableLog,false,"Enable MotionManager logging");

    rawStream = NULL;
    ballStream = NULL;

    state = STATE_IDLE;
}

Hockey::~Hockey()
{
    if(rawStream != NULL)
    {
        delete rawStream;
        rawStream = NULL;
    }
    if(ballStream != NULL)
    {
        delete ballStream;
        ballStream = NULL;
    }
}

bool Hockey::Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition)
{
    (void)iniFilePath;

    if(!InitIni(this->iniFilePath.c_str()))
        return false;

    if(!visionOnly)
        if(!InitCM730(motionFilePath,safePosition))
            return false;

    LoadIniSettings();

    if(!InitCamera(this->iniFilePath.c_str()))
    {
        cerr << "FAILED TO INITIALIZE CAMERA" << endl;
        if(visionOnly)
            return false;
    }
    else
        StartVisionThread();

    Voice::Initialize();

    if(!visionOnly)
    {
        cout << "Standing Up" << endl;
        Action::GetInstance()->Start(FOREHAND_SHOT_READY);
        Action::GetInstance()->Finish();
        cout << "Done" << endl;

        cout << "Setting up Skating module..." << endl;
        Skating::GetInstance()->m_Joint.SetEnableBody(false);
        Skating::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
        MotionManager::GetInstance()->AddModule((MotionModule*)Skating::GetInstance());
        Skating::GetInstance()->LoadINISettings(ini);
        //int MIN_ROW = ini->geti("Ball","Top");
        //int MAX_ROW = ini->geti("Ball","Bottom");
        //int MIN_COL = ini->geti("Ball","Left");
        //int MAX_COL = ini->geti("Ball","Right");
        //int LR_THRESHOLD = ini->geti("Ball","LRThreshold");
        cout << "done" << endl << endl;

        Skating::GetInstance()->X_MOVE_AMPLITUDE = ini->geti("Movement","StrideLength");
        Skating::GetInstance()->A_MOVE_AMPLITUDE = ini->geti("Movement","Turn");

        Head::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(0,-10);
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
    }

    return true;
}

void Hockey::LoadIniSettings()
{
    cout << "Loading colour profile from section Yellow" << endl;
    ball.LoadIniSettings(*ini,"Yellow");
    cout << "Done";

    if(recordVideo)
    {
        ballStream = new VideoWriter(ini->gets("Files","Ball","ball.avi"),CV_FOURCC('D', 'I', 'V', 'X'), 10, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        rawStream = new VideoWriter(ini->gets("Files","Raw","raw.avi"),CV_FOURCC('D', 'I', 'V', 'X'), 10, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }

    MIN_RANGE = ini->getd("Vision","MinRange");
    MAX_RANGE = ini->getd("Vision","MaxRange");
    THRESHOLD_ANGLE = ini->getd("Vision","Threshold");
    LEFT_ANGLE = ini->getd("Vision","Left");
    RIGHT_ANGLE = ini->getd("Vision","Right");
}

void Hockey::Process()
{
    // keep hitting the ball forever
    handleButton();

    switch(state)
    {
        case STATE_IDLE:
            Skating::GetInstance()->Stop();
            break;

        case STATE_MOVING:
            Skating::GetInstance()->Start();
            Skating::GetInstance()->X_MOVE_AMPLITUDE = ini->geti("Movement","StrideLength");
            Skating::GetInstance()->A_MOVE_AMPLITUDE = ini->geti("Movement","Turn");

            if(ball.WasFound())
            {
                if(range >= MIN_RANGE && range <= MAX_RANGE &&
                    angle <= LEFT_ANGLE && angle >= RIGHT_ANGLE)
                {
                    cout << "Target might be in position; stopping" << endl;
                    // stop skating and get ready to shoot
                    state = STATE_STOPPING;

                }

            }// wasFound()

            break;

        case STATE_STOPPING:
            Skating::GetInstance()->Stop();
            Skating::GetInstance()->Finish();

            state = STATE_SHOOTING;
            break;

        case STATE_SHOOTING:
            // swing the stick and hit the ball
            if(angle > THRESHOLD_ANGLE)
            {
                //Voice::Speak("Shooting Backhand");
                cout << "Shooting Backhand" << endl;
                Action::GetInstance()->Start(BACKHAND_SHOT_GO);
            }
            else
            {
                //Voice::Speak("Shooting Forehand");
                cout << "Shooting Forehand" << endl;
                Action::GetInstance()->Start(FOREHAND_SHOT_GO);
            }
            Action::GetInstance()->Finish();
            cout << "done" << endl;

            //Action::GetInstance()->Start(FOREHAND_SHOT_READY);
            //Head::GetInstance()->MoveByAngle(0,0);

            state = STATE_MOVING;
            break;

    }
}

void Hockey::Execute()
{
    for(;;)
    {
        Process();
    }
}

void Hockey::handleButton()
{
    if(MotionStatus::BUTTON==lastButton)
        return;
    else
    {
        cout << "[INFO] Button pressed" << endl;
        lastButton = MotionStatus::BUTTON;

        if(lastButton == CM730::MIDDLE_BUTTON)
        {
            cout << "[INFO] Skating module started" << endl;
            state = STATE_MOVING;
        }
        else if(state!=STATE_IDLE && lastButton == CM730::LEFT_BUTTON)
        {
            cout << "[INFO] Entering idle state" << endl;
            state = STATE_IDLE;
        }
    }
}

void Hockey::HandleVideo()
{
    static char buffer[255];
        static cv::Point pt;
        static cv::Scalar colour(255,255,255);

        CvCamera::GetInstance()->CaptureFrame();
        Mat blur = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
        Mat ballScanline = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
        //Mat stickScanline = Mat::zeros(CvCamera::GetInstance()->img.rows,CvCamera::GetInstance()->img.cols,CvCamera::GetInstance()->img.type());
        GaussianBlur(CvCamera::GetInstance()->yuvFrame, blur, Size(3,3), 0);

        if(ball.FindInFrame(blur,&ballScanline))
        {
            range = ball.GetBoundingBox()->GetRange(SKATE_HEIGHT);
            angle = ball.GetBoundingBox()->GetAngle(SKATE_HEIGHT);
            //cout << range << " " << angle << " " << Head::GetInstance()->GetPanAngle() << endl;

            if(recordVideo || showVideo)
            {
                ball.Draw(CvCamera::GetInstance()->rgbFrame);
                ball.Draw(ballScanline);
                //ball.Print();

                string lr;
                string fb;

                if(angle > LEFT_ANGLE)
                    lr = "<--";
                else if(angle <= LEFT_ANGLE && angle >= THRESHOLD_ANGLE)
                    lr = "|<|";
                else if(angle <= THRESHOLD_ANGLE && angle >= RIGHT_ANGLE)
                    lr = "|>|";
                else
                    lr = "-->";

                if(range < MIN_RANGE)
                    fb = "v";
                else if(range >= MIN_RANGE && range <= MAX_RANGE)
                    fb = "--";
                else
                    fb = "^";

                // annotate the ball's range and angle data into the frame
                sprintf(buffer, "Range: %0.2fmm %s", range, fb.c_str());
                pt = cv::Point(0,12);
                cv::putText(ballScanline, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
                cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

                sprintf(buffer, "Angle: %0.2f %s", angle, lr.c_str());
                pt = cv::Point(0,24);
                cv::putText(ballScanline, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
                cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            }
        }
        else
        {
            if(showVideo || recordVideo)
            {
                // placeholder text
                pt = cv::Point(0,12);
                cv::putText(ballScanline, "Range:",  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

                pt = cv::Point(0,24);
                cv::putText(ballScanline, "Angle:",  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            }
        }

        if(recordVideo)
        {
            (*rawStream) << CvCamera::GetInstance()->rgbFrame;
            (*ballStream) << ballScanline;
        }

        if(showVideo)
        {
            cv::imshow("Webcam",CvCamera::GetInstance()->rgbFrame);
            cv::imshow("Ball",ballScanline);
            //cv::imshow("Stick",stickScanline);
            cv::waitKey(1);
        }
}
