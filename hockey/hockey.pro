TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

OBJECTS_DIR=build

LIBS += -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ldarwin \

SOURCES += main.cpp \
    Hockey.cpp \
    Skating.cpp

HEADERS += \
    Hockey.h \
    Skating.h

OTHER_FILES += \
    motion.bin \
    config.ini

