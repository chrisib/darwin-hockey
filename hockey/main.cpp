#include <iostream>
#include <Hockey.h>
#include <darwin/framework/Action.h>

using namespace std;

int main(int argc, char **argv)
{
    Hockey *hockey = new Hockey();
    hockey->ParseArguments(argc, argv);
    hockey->Initialize("config.ini","motion.bin",Robot::Action::DEFAULT_MOTION_SIT_DOWN);

    hockey->Execute();

    return 0;
}

